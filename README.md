# Usage
./refresh.py yourCookie.txt

ps. yourCookie.txt stores _document.cookie_ from your browser at the first line **without** quotation marks.

# Requirements
* requests
* beautifulsoup4 

